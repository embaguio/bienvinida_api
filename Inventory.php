<?php 

require_once('SQLHelper.php');

class Inventory {

	private $sql_obj = null;

	public function __construct(){

		$this->sql_obj = SQLHelper::get_instance();
	}

	public function getItems() {
		$return = array();
		$return['success'] = false;

 		$sql = "CALL get_items_inventory()";
		$result = $this->sql_obj->CALL($sql);
		 
 		if($result){
			$final_data = array();
			while($row = mysqli_fetch_assoc($result)){
				$final_data[] = $row;
			}

 			$return['success'] = $final_data && sizeof($final_data);
 			$return['data'] = $final_data;
		}
		 
 		echo json_encode($return);
	}

	public function getCategory() {
		$return = array();
		$return['success'] = false;

 		$sql = "CALL get_inventory_category()";
		$result = $this->sql_obj->CALL($sql);
		 
 		if($result){
			$final_data = array();
			while($row = mysqli_fetch_assoc($result)){
				$final_data[] = $row;
			}

 			$return['success'] = $final_data && sizeof($final_data);
 			$return['data'] = $final_data;
		}
		 
 		echo json_encode($return);
	}

	public function getMeasurement() {
		$return = array();
		$return['success'] = false;

 		$sql = "CALL get_unit_of_measurement()";
		$result = $this->sql_obj->CALL($sql);
		 
 		if($result){
			$final_data = array();
			while($row = mysqli_fetch_assoc($result)){
				$final_data[] = $row;
			}

 			$return['success'] = $final_data && sizeof($final_data);
 			$return['data'] = $final_data;
		}
		 
 		echo json_encode($return);
	}

	public function getItemType() {
		$return = array();
		$return['success'] = false;

 		$sql = "CALL get_unit_of_measurement()";
		$result = $this->sql_obj->CALL($sql);
		 
 		if($result){
			$final_data = array();
			while($row = mysqli_fetch_assoc($result)){
				$final_data[] = $row;
			}

 			$return['success'] = $final_data && sizeof($final_data);
 			$return['data'] = $final_data;
		}
		 
 		echo json_encode($return);
	}

	public function addItem($data){
		$return = array();
		$return['success'] = false;

		$sql = "CALL add_item_inventory('".$data['is_consumable']."','".$data['sku']."','".$data['name']."','".$data['measurement']."','".$data['modifiedBy']."','".$data['id_inventory_category']."','".$data['item_inventory_actual_measurement']."','".$data['item_inventory_qty']."','{$data['item_price']}')";
		if($this->sql_obj->CALL($sql)){
			$return['success'] = true;
		}
		echo json_encode($return);
	}


	public function update($data){
		$return = array();
		$return['success'] = false;

		$sql = "CALL update_item_inventory('".$data['id_item_inventory']."','".$data['is_consumable']."','".$data['sku']."','".$data['name']."','".$data['unit_measure']."','".$data['modified_by']."','".$data['id_inventory_category']."','".$data['item_inventory_actual_measurement']."')";
		if($this->sql_obj->CALL($sql)){
			$return['success'] = true;
		}
		echo json_encode($return);
	}

	public function updateItemStatus($data){
		$return = array();
		$return['success'] = false;

		$sql = "CALL update_item_status('".$data['id_item_inventory']."','".$data['is_active']."')";

		if($this->sql_obj->CALL($sql)){
			$return['success'] = true;
		}
		echo json_encode($return);
	}
}



