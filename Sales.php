<?php 
require 'vendor/autoload.php';
require_once 'vendor/dompdf/dompdf/lib/html5lib/Parser.php';
require_once 'vendor/dompdf/dompdf/src/Autoloader.php';
Dompdf\Autoloader::register();
use Dompdf\Dompdf;

require_once('SQLHelper.php');

class Sales {

	private $sql_obj = null;

	public function __construct(){

		$this->sql_obj = SQLHelper::get_instance();
	}

	public function get() {
		$return = array();
		$return['success'] = false;

 		$sql = "CALL get_sales_order()";
		$result = $this->sql_obj->CALL($sql);
		 
 		if($result){
			$final_data = array();
			while($row = mysqli_fetch_assoc($result)){
				$final_data[] = $row;
			}

 			$return['success'] = $final_data && sizeof($final_data);
 			$return['data'] = $final_data;
		}
		 
 		echo json_encode($return);
	}


	public function add($data){
		$return = array();
		$return['success'] = false;

		$sql = "CALL add_sales_order('{$data['customer_id']}','{$data['sales_order_description']}','{$data['sales_order_posting']}','{$data['sales_order_total']}','{$data['modified_by']}',@out_sales_order_id)";

		$result = $this->sql_obj->CALL_OUT($sql, 'out_sales_order_id');

 		if($result){
 			$return['success'] = true;
 			$return['data'] = $result;
		}

		echo json_encode($return);
	}

	public function addLines($data){
		$return = array();
		$return['success'] = false;
		
		foreach ($data as $line) {
			if (isset($line['id_item_inventory'])) {
			
				$sql = "CALL add_sales_line('{$data['sales_order_id']}','{$line['id_item_inventory']}','{$line['sales_line_qty']}','{$line['sales_order_discount_rate']}','{$line['sales_line_total']}')";
	
				if($this->sql_obj->CALL($sql)){
					$return['success'] = true;
				}
			}
		}

		echo json_encode($return);
	}

	public function updatePO($data){
		$return = array();
		$return['success'] = false;

		$sql = "CALL update_po('{$data['sales_order_id']}','{$data['customer_id']}','{$data['sales_order_description']}','{$data['po_grand_total']}','{$data['sales_order_posting']}','{$data['po_due_date']}','{$data['modified_by']}')";

		if($this->sql_obj->CALL($sql)){
			$return['success'] = true;
		}
		echo json_encode($return);
	}


	public function getLines($data, $isJSON = false){
		$return = array();
		$return['success'] = false;

		$sql = "CALL get_sales_line('{$data['sales_order_id']}')";
		$result = $this->sql_obj->CALL($sql);
		 
 		if($result){
			$final_data = array();
			while($row = mysqli_fetch_assoc($result)){
				$final_data[] = $row;
			}

 			$return['success'] = $final_data && sizeof($final_data);
 			$return['data'] = $final_data;
		}
		if ($isJSON) {
			return $return;
		} else {
			echo json_encode($return);
		}
	}
	

	public function updateStatus($data){
		$return = array();
		$return['success'] = false;

		$sql = "CALL update_po_status('{$data['sales_order_id']}', '{$data['is_active']}')";

		if($this->sql_obj->CALL($sql)){
			$return['success'] = true;
		}
		echo json_encode($return);
	}

	public function deleteLine($data){
		$return = array();
		$return['success'] = false;

		$sql = "CALL delete_po_line('{$data['purchase_order_line_id']}')";

		if($this->sql_obj->CALL($sql)){
			$return['success'] = true;
		}
		echo json_encode($return);
	}

	public function update($data){
		$return = array();
		$return['success'] = false;

		$sql = "CALL update_supplier('".$data['sales_order_id']."','".$data['supplier_name']."','".$data['supplier_address']."','".$data['supplier_contact']."','".$data['supplier_reference']."')";
		
		if($this->sql_obj->CALL($sql)){
			$return['success'] = true;
		}
		echo json_encode($return);
	}

	public function print($data) {
		$return = array();
		$return['success'] = false;
		$generatedDate = date('m/d/Y');
		$grandTotal = number_format($data['sales_order_total'], 2);
		$status = $data["is_open"] === "1" ? "Open" : "Cancelled";
		$rows = $this->getLines($data, true)['data'];

		$buildRows = '';

		foreach($rows as $line) {
			// var_dump($line);
			// die();

			$buildRows.="
				<tr>
					<td>{$line['sku']}</td>
					<td>{$line['name']}</td>
					<td>{$line['sales_line_qty']}</td>
					<td>{$line['item_price']}</td>
					<td>{$line['sales_order_discount_rate']}</td>
					<td>".number_format($line['sales_line_total'], 2)."</td>
				</tr>";
		}


		$htmlString = "<!DOCTYPE html>
		<html>
			<style>
				body {
					font-family: sans-serif;
      				margin: -20px 0px;
				}
				.pull-right {
					float: right;
				}
				.gen-details span {
					margin-right: 20px;
				}
				.company {
					background: #4292bb;
					color: white;
					padding: 20px;
				}
				.header {
					font-weight: bold;
					background: #4292bb;
					color: white;

				}

				td {
					padding: 10px;
				}

				tr:nth-child(even) {background-color: #f2f2f2;}
			</style>
		<body>
		
			<h3 class='company'>Bienvinida's Sales Invoice <span class='pull-right'>Date Generated: {$generatedDate}</span></h3>
			<div class='gen-details'>
				<h4>Customer: {$data['customer_name']}</h4>
				<span><b>Ordered Date:</b> {$data['sales_order_posting']}</span>
				<span><b>Description:</b> {$data['sales_order_description']}</span>
				<span><b>Status:</b> {$status}</span>
			</div>
			<hr>
		
		
			<table style='width:100%'>
				<tr class='header'>
					<td>SKU</td>
					<td>Item Description</td>
					<td>Quantity</td>
					<td>Original Price</td>
					<td>Item Discount %</td>
					<td>Sub Total</td>
				</tr>
				{$buildRows}
			</table>
	
			<hr>
			<p style='text-align: right;'><b>Grand Total: </b> {$grandTotal}</p>
		
		</body>
		
		</html>";
		
		$curr_date =  date('Y-m-d');

		$dompdf = new DOMPDF();
		$dompdf->load_html($htmlString);
		$dompdf->render();
		$output = $dompdf->output();

		$saveLink = "printables/SO-{$curr_date}.pdf";
		file_put_contents($saveLink, $output);

		$base_url= "http://".$_SERVER['SERVER_NAME'].dirname($_SERVER['PHP_SELF']);

		$return['success'] = true;
		$return['data'] = $base_url."/".$saveLink;

		echo json_encode($return, JSON_UNESCAPED_SLASHES);
	}

	public function cancel($data) {
		$return = array();
		$return['success'] = false;
		$generatedDate = date('m/d/Y');

		$curr_date =  date('Y-m-d');

		$sql = "CALL cancel_order('{$data['sales_order_id']}')";
		
		if(!($this->sql_obj->CALL($sql))){
			$return['success'] = false;
			die();
		}
		
		$rows = $this->getLines($data, true)['data'];
		foreach($rows as $line) {
			$sql = "CALL update_cancel_item('{$line['id_item_inventory']}', '{$line['sales_line_qty']}')";
		
			if($this->sql_obj->CALL($sql)){
				$return['success'] = true;
			} else {
				$return['success'] = false;
			}
		}

		echo json_encode($return, JSON_UNESCAPED_SLASHES);
	}
}



