<?php 

require_once('SQLHelper.php');

class Customer {

	private $sql_obj = null;

	public function __construct(){

		$this->sql_obj = SQLHelper::get_instance();
	}

	public function getCustomers() {
		$return = array();
		$return['success'] = false;

 		$sql = "CALL get_customers()";
		$result = $this->sql_obj->CALL($sql);
		 
 		if($result){
			$final_data = array();
			while($row = mysqli_fetch_assoc($result)){
				$final_data[] = $row;
			}

 			$return['success'] = $final_data && sizeof($final_data);
 			$return['data'] = $final_data;
		}
		 
 		echo json_encode($return);
	}


	public function add($data){
		$return = array();
		$return['success'] = false;

		$sql = "CALL add_customer('".$data['customer_name']."','".$data['customer_address']."','".$data['customer_contact']."')";

		if($this->sql_obj->CALL($sql)){
			$return['success'] = true;
		}
		echo json_encode($return);
	}

	public function delete($data){
		$return = array();
		$return['success'] = false;

		$sql = "CALL delete_customer('".$data['customer_id']."')";

		if($this->sql_obj->CALL($sql)){
			$return['success'] = true;
		}
		echo json_encode($return);
	}

	public function update($data){
		$return = array();
		$return['success'] = false;

		$sql = 'CALL update_customer("'.$data["customer_name"].'","'.$data["customer_address"].'","'.$data["customer_contact"].'","'.$data["customer_id"].'")';
		
		if($this->sql_obj->CALL($sql)){
			$return['success'] = true;
		}
		echo json_encode($return);
	}

}



