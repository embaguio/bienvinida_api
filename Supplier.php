<?php 

require_once('SQLHelper.php');

class Supplier {

	private $sql_obj = null;

	public function __construct(){

		$this->sql_obj = SQLHelper::get_instance();
	}

	public function getSuppliers() {
		$return = array();
		$return['success'] = false;

 		$sql = "CALL get_suppliers()";
		$result = $this->sql_obj->CALL($sql);
		 
 		if($result){
			$final_data = array();
			while($row = mysqli_fetch_assoc($result)){
				$final_data[] = $row;
			}

 			$return['success'] = $final_data && sizeof($final_data);
 			$return['data'] = $final_data;
		}
		 
 		echo json_encode($return);
	}


	public function add($data){
		$return = array();
		$return['success'] = false;

		$sql = "CALL add_supplier('".$data['supplier_name']."','".$data['supplier_address']."','".$data['supplier_contact']."','".$data['supplier_reference']."')";

		if($this->sql_obj->CALL($sql)){
			$return['success'] = true;
		}
		echo json_encode($return);
	}

	public function delete($data){
		$return = array();
		$return['success'] = false;

		$sql = "CALL delete_supplier('".$data['supplier_id']."')";

		if($this->sql_obj->CALL($sql)){
			$return['success'] = true;
		}
		echo json_encode($return);
	}

	public function update($data){
		$return = array();
		$return['success'] = false;

		$sql = "CALL update_supplier('".$data['supplier_id']."','".$data['supplier_name']."','".$data['supplier_address']."','".$data['supplier_contact']."','".$data['supplier_reference']."')";
		
		if($this->sql_obj->CALL($sql)){
			$return['success'] = true;
		}
		echo json_encode($return);
	}

}



