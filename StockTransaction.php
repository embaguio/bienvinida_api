<?php 
require 'vendor/autoload.php';
require_once 'vendor/dompdf/dompdf/lib/html5lib/Parser.php';
require_once 'vendor/dompdf/dompdf/src/Autoloader.php';
Dompdf\Autoloader::register();
use Dompdf\Dompdf;

require_once('SQLHelper.php');

class StockTransaction {

	private $sql_obj = null;

	public function __construct(){

		$this->sql_obj = SQLHelper::get_instance();
	}

	public function get($id) {
		$return = array();
		$return['success'] = false;

		$params = isset($id) && sizeof($id) ? $id : 'NULL';
		
 		$sql = "CALL get_stock_transaction({$params})";
		$result = $this->sql_obj->CALL($sql);
		 
 		if($result){
			$final_data = array();
			while($row = mysqli_fetch_assoc($result)){
				$final_data[] = $row;
			}

 			$return['success'] = $final_data && sizeof($final_data);
 			$return['data'] = $final_data;
		}
		 
 		echo json_encode($return);
	}


	public function add($data){
		$return = array();
		$return['success'] = false;

		$sql = "CALL add_stock_transaction('{$data['stock_transaction_date']}', '{$data['stock_transaction_description']}', '{$data['stock_transaction_total']}', '{$data['transaction_type_id']}','{$data['modified_by']}','{$data['supplier_id']}','{$data['id_inventory_category']}', @out_stock_transaction)";

		$result = $this->sql_obj->CALL_OUT($sql, 'out_stock_transaction');

 		if($result){
 			$return['success'] = true;
 			$return['data'] = $result;
		}

		echo json_encode($return);
	}

	public function updatePO($data){
		$return = array();
		$return['success'] = false;

		$sql = "CALL update_po('{$data['purchase_order_id']}','{$data['supplier_id']}','{$data['purchase_order_description']}','{$data['po_grand_total']}','{$data['purchase_order_posting']}','{$data['po_due_date']}','{$data['modified_by']}')";

		if($this->sql_obj->CALL($sql)){
			$return['success'] = true;
		}
		echo json_encode($return);
	}

	public function addLines($data){
		$return = array();
		$return['success'] = false;
		foreach ($data as $line) {
			if (isset($line['id_item_inventory'])) {
			
				$sql = "CALL add_stock_transaction_line('{$data['stock_transaction_id']}','{$line['id_item_inventory']}','{$line['stock_transaction_line_qty']}', '{$data['transaction_type_id']}')";
	
				if($this->sql_obj->CALL($sql)){
					$return['success'] = true;
				}
			}
		}

		echo json_encode($return);
	}

	public function updateLines($data){
		$return = array();
		$return['success'] = false;
		foreach ($data as $line) {
			if (isset($line['id_item_inventory']) && isset($line['purchase_order_line_id'])) {
			
				$sql = "CALL update_po_line('{$line['purchase_order_line_id']}','{$line['id_item_inventory']}','{$line['expected_qty']}','{$line['po_unit_price']}','{$line['po_total_price']}','{$line['purchase_order_id']}')";
	
				if($this->sql_obj->CALL($sql)){
					$return['success'] = true;
				}
			}
		}

		echo json_encode($return);
	}

	public function getLines($data, $isJSON = false){
		$return = array();
		$return['success'] = false;

		$sql = "CALL get_stock_transaction_lines('{$data['stock_transaction_id']}')";
		$result = $this->sql_obj->CALL($sql);
		 
 		if($result){
			$final_data = array();
			while($row = mysqli_fetch_assoc($result)){
				$final_data[] = $row;
			}

 			$return['success'] = $final_data && sizeof($final_data);
 			$return['data'] = $final_data;
		}
		if ($isJSON) {
			return $return;
		} else {
			echo json_encode($return);
		}
	}

	public function getAdjustments() {
		$return = array();
		$return['success'] = false;

		$params = isset($id) && sizeof($id) ? $id : 'NULL';
		
 		$sql = "CALL get_stock_adjustments()";
		$result = $this->sql_obj->CALL($sql);
		 
 		if($result){
			$final_data = array();
			while($row = mysqli_fetch_assoc($result)){
				$final_data[] = $row;
			}

 			$return['success'] = $final_data && sizeof($final_data);
 			$return['data'] = $final_data;
		}
		 
 		echo json_encode($return);
	}

	public function adjustment($data){
		$return = array();
		$return['success'] = false;

		$sql = "CALL add_stock_adjustment('{$data['id_item_inventory']}', '{$data['system_count']}', '{$data['adjustment_count']}', '{$data['adjustment_remarks']}','{$data['modified_by']}')";

		if($this->sql_obj->CALL($sql)){
			$return['success'] = true;
		}
		echo json_encode($return);
	}

	public function print($data) {
		$return = array();
		$return['success'] = false;
		$generatedDate = date('m/d/Y');
		$rows = $this->getLines($data, true)['data'];

		$buildRows = '';
		
		foreach($rows as $line) {
			$buildRows.="
				<tr>
					<td>{$line['sku']}</td>
					<td>{$line['name']}</td>
					<td>{$line['stock_transaction_line_qty']}</td>
				</tr>";
		}


		$htmlString = "<!DOCTYPE html>
		<html>
			<style>
				body {
					font-family: sans-serif;
      				margin: -20px 0px;
				}
				.pull-right {
					float: right;
				}
				.gen-details span {
					margin-right: 20px;
				}
				.company {
					background: #4292bb;
					color: white;
					padding: 20px;
				}
				.header {
					font-weight: bold;
					background: #4292bb;
					color: white;

				}

				td {
					padding: 10px;
				}

				tr:nth-child(even) {background-color: #f2f2f2;}
			</style>
		<body>
		
			<h3 class='company'>Bienvinida's {$data['transaction_type_description']} Transaction <span class='pull-right'>Date Generated: {$generatedDate}</span></h3>
			<div class='gen-details'>
				<span><b>Stocking Date:</b> {$data['stock_transaction_date']}</span>
				<span><b>Description:</b> {$data['stock_transaction_description']}</span>
			</div>
			<hr>
		
		
			<table style='width:100%'>
				<tr class='header'>
					<td>SKU</td>
					<td>Item Description</td>
					<td>Quantity</td>
				</tr>
				{$buildRows}
			</table>
	
			<hr>
			<p style='text-align: right;'><b>Stock Transaction Total: </b> {$data['stock_transaction_total']}</p>
		
		</body>
		
		</html>";
		
		$curr_date =  date('Y-m-d');

		$dompdf = new DOMPDF();
		$dompdf->load_html($htmlString);
		$dompdf->render();
		$output = $dompdf->output();

		$saveLink = "printables/STOCK_IN-{$curr_date}.pdf";
		file_put_contents($saveLink, $output);

		$base_url= "http://".$_SERVER['SERVER_NAME'].dirname($_SERVER['PHP_SELF']);

		$return['success'] = true;
		$return['data'] = $base_url."/".$saveLink;

		echo json_encode($return, JSON_UNESCAPED_SLASHES);
	}

}



