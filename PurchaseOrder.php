<?php 
require 'vendor/autoload.php';
require_once 'vendor/dompdf/dompdf/lib/html5lib/Parser.php';
require_once 'vendor/dompdf/dompdf/src/Autoloader.php';
Dompdf\Autoloader::register();
use Dompdf\Dompdf;

require_once('SQLHelper.php');

class PurchaseOrder {

	private $sql_obj = null;

	public function __construct(){

		$this->sql_obj = SQLHelper::get_instance();
	}

	public function get() {
		$return = array();
		$return['success'] = false;

 		$sql = "CALL get_purchase_orders()";
		$result = $this->sql_obj->CALL($sql);
		 
 		if($result){
			$final_data = array();
			while($row = mysqli_fetch_assoc($result)){
				$final_data[] = $row;
			}

 			$return['success'] = $final_data && sizeof($final_data);
 			$return['data'] = $final_data;
		}
		 
 		echo json_encode($return);
	}


	public function add($data){
		$return = array();
		$return['success'] = false;

		$sql = "CALL add_purchase_order('{$data['supplier_id']}','{$data['purchase_order_description']}','{$data['po_grand_total']}','{$data['purchase_order_posting']}','{$data['po_due_date']}','{$data['modified_by']}',@out_purchase_order_id)";

		$result = $this->sql_obj->CALL_OUT($sql, 'out_purchase_order_id');

 		if($result){
 			$return['success'] = true;
 			$return['data'] = $result;
		}

		echo json_encode($return);
	}

	public function updatePO($data){
		$return = array();
		$return['success'] = false;

		$sql = "CALL update_po('{$data['purchase_order_id']}','{$data['supplier_id']}','{$data['purchase_order_description']}','{$data['po_grand_total']}','{$data['purchase_order_posting']}','{$data['po_due_date']}','{$data['modified_by']}')";

		if($this->sql_obj->CALL($sql)){
			$return['success'] = true;
		}
		echo json_encode($return);
	}

	public function addLines($data){
		$return = array();
		$return['success'] = false;
		foreach ($data as $line) {
			if (isset($line['id_item_inventory'])) {
			
				$sql = "CALL add_po_line('{$data['purchase_order_id']}','{$line['id_item_inventory']}','{$line['expected_qty']}','{$line['po_unit_price']}','{$line['po_total_price']}')";
	
				if($this->sql_obj->CALL($sql)){
					$return['success'] = true;
				}
			}
		}

		echo json_encode($return);
	}

	public function updateLines($data){
		$return = array();
		$return['success'] = false;
		foreach ($data as $line) {
			if (isset($line['id_item_inventory']) && isset($line['purchase_order_line_id'])) {
			
				$sql = "CALL update_po_line('{$line['purchase_order_line_id']}','{$line['id_item_inventory']}','{$line['expected_qty']}','{$line['po_unit_price']}','{$line['po_total_price']}','{$line['purchase_order_id']}')";
	
				if($this->sql_obj->CALL($sql)){
					$return['success'] = true;
				}
			}
		}

		echo json_encode($return);
	}

	public function getLines($data, $isJSON = false){
		$return = array();
		$return['success'] = false;

		$sql = "CALL get_po_lines('{$data['purchase_order_id']}')";
		$result = $this->sql_obj->CALL($sql);
		 
 		if($result){
			$final_data = array();
			while($row = mysqli_fetch_assoc($result)){
				$final_data[] = $row;
			}

 			$return['success'] = $final_data && sizeof($final_data);
 			$return['data'] = $final_data;
		}
		if ($isJSON) {
			return $return;
		} else {
			echo json_encode($return);
		}
	}
	

	public function updateStatus($data){
		$return = array();
		$return['success'] = false;

		$sql = "CALL update_po_status('{$data['purchase_order_id']}', '{$data['is_active']}')";

		if($this->sql_obj->CALL($sql)){
			$return['success'] = true;
		}
		echo json_encode($return);
	}

	public function deleteLine($data){
		$return = array();
		$return['success'] = false;

		$sql = "CALL delete_po_line('{$data['purchase_order_line_id']}')";

		if($this->sql_obj->CALL($sql)){
			$return['success'] = true;
		}
		echo json_encode($return);
	}

	public function update($data){
		$return = array();
		$return['success'] = false;

		$sql = "CALL update_supplier('".$data['purchase_order_id']."','".$data['supplier_name']."','".$data['supplier_address']."','".$data['supplier_contact']."','".$data['supplier_reference']."')";
		
		if($this->sql_obj->CALL($sql)){
			$return['success'] = true;
		}
		echo json_encode($return);
	}

	public function print($data) {
		$return = array();
		$return['success'] = false;
		$generatedDate = date('m/d/Y');
		$grandTotal = number_format($data['po_grand_total'], 2);
		$rows = $this->getLines($data, true)['data'];

		$buildRows = '';

		foreach($rows as $line) {
			$buildRows.="
				<tr>
					<td>{$line['sku']}</td>
					<td>{$line['name']}</td>
					<td>{$line['expected_qty']}</td>
					<td>".number_format($line['po_unit_price'], 2)."</td>
					<td>".number_format($line['po_total_price'], 2)."</td>
				</tr>";
		}


		$htmlString = "<!DOCTYPE html>
		<html>
			<style>
				body {
					font-family: sans-serif;
      				margin: -20px 0px;
				}
				.pull-right {
					float: right;
				}
				.gen-details span {
					margin-right: 20px;
				}
				.company {
					background: #4292bb;
					color: white;
					padding: 20px;
				}
				.header {
					font-weight: bold;
					background: #4292bb;
					color: white;

				}

				td {
					padding: 10px;
				}

				tr:nth-child(even) {background-color: #f2f2f2;}
			</style>
		<body>
		
			<h3 class='company'>Bienvinida's Purchase Order <span class='pull-right'>Date Generated: {$generatedDate}</span></h3>
			<div class='gen-details'>
				<h4>Supplier: {$data['supplier_name']}</h4>
				<span><b>Ordered Date:</b> {$data['purchase_order_posting']}</span>
				<span><b>Due Date:</b> {$data['po_due_date']}</span>
				<span><b>Description:</b> {$data['purchase_order_description']}</span>
			</div>
			<hr>
		
		
			<table style='width:100%'>
				<tr class='header'>
					<td>SKU</td>
					<td>Item Description</td>
					<td>Quantity</td>
					<td>Unit Price</td>
					<td>Sub Total</td>
				</tr>
				{$buildRows}
			</table>
	
			<hr>
			<p style='text-align: right;'><b>Grand Total: </b> {$grandTotal}</p>
		
		</body>
		
		</html>";
		
		$curr_date =  date('Y-m-d');

		$dompdf = new DOMPDF();
		$dompdf->load_html($htmlString);
		$dompdf->render();
		$output = $dompdf->output();

		$saveLink = "printables/PO-{$curr_date}.pdf";
		file_put_contents($saveLink, $output);

		$base_url= "http://".$_SERVER['SERVER_NAME'].dirname($_SERVER['PHP_SELF']);

		$return['success'] = true;
		$return['data'] = $base_url."/".$saveLink;

		echo json_encode($return, JSON_UNESCAPED_SLASHES);
	}

}



