<?php 

require_once('SQLHelper.php');

class Extra {

	private $sql_obj = null;

	public function __construct(){

		$this->sql_obj = SQLHelper::get_instance();
	}

	public function addMeasurement($data){
		$return = array();
		$return['success'] = false;

		$sql = "CALL add_measurement('{$data['name']}', '{$data['label']}')";

		if($this->sql_obj->CALL($sql)){
			$return['success'] = true;
		}
		echo json_encode($return);
	}

	public function addExpenseCategory($data){
		$return = array();
		$return['success'] = false;

		$sql = "CALL add_expense_category('{$data['expense_type_description']}')";

		if($this->sql_obj->CALL($sql)){
			$return['success'] = true;
		}
		echo json_encode($return);
	}

	public function delete($data){
		$return = array();
		$return['success'] = false;

		$sql = "CALL delete_customer('".$data['customer_id']."')";

		if($this->sql_obj->CALL($sql)){
			$return['success'] = true;
		}
		echo json_encode($return);
	}

	public function update($data){
		$return = array();
		$return['success'] = false;

		$sql = 'CALL update_customer("'.$data["customer_name"].'","'.$data["customer_address"].'","'.$data["customer_contact"].'","'.$data["customer_id"].'")';
		
		if($this->sql_obj->CALL($sql)){
			$return['success'] = true;
		}
		echo json_encode($return);
	}

}



