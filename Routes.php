<?php

class Routes {
	public static function executeRoute($route) {
		$tmpRoute = str_replace( "-", "", $route["module"] ); 
		$modulePath = ucwords($tmpRoute).".php";
		$module = strtolower($route["module"]);
		$action = strtolower($route["action"]);

		
		if(!file_exists($modulePath)){
			Routes::handleError();
		}

		require_once($modulePath);
		Routes::executeAction($module, $action);
	}

	protected static function executeAction($module, $action) {
		$endpoint = "/".$module."/".$action;

		if($module == "user"){
			$action = new User();
		} else if($module == "inventory"){
			$action = new Inventory();
		} else if($module == "customer"){
			$action = new Customer();
		} else if($module == "supplier"){
			$action = new Supplier();
		} else if($module == "purchase-order"){
			$action = new PurchaseOrder();
		} else if($module == "stock-transaction"){
			$action = new StockTransaction();
		} else if($module == "expense"){
			$action = new Expense();
		} else if($module == "sales"){
			$action = new Sales();
		} else if($module == "reports"){
			$action = new Reports();
		} else if($module == "extra"){
			$action = new Extra();
		}
		// else { 
		// 	$action = new Statics();
		// }
		
		switch ($endpoint) {
			/*
			** THIS CONSIST OF ROUTES FROM ENDPOINT URL
			** THIS WILL ONLY CALL CORRESPONDING METHODS IN CLASS
			*/
			case '/user/login':
					$action->login($_POST['username'], $_POST['password']);
				break;

			case '/user/get-users':
					$action->getUsers();
				break;
			
			case '/user/get-user-roles':
				$action->getUserRoles();
			break;
			
			case '/user/add':
				$action->add($_POST);
			break;
			
			case '/user/delete':
				$action->delete($_POST);
			break;
			
			case '/user/update':
				$action->update($_POST);
			break;

			case '/user/update-password':
				$action->updatePassword($_POST);
			break;
		
			


			//start of inventory route	
			case '/inventory/get-items':
				$action->getItems();
			break;

			case '/inventory/get-categories':
				$action->getCategory();
			break;

			case '/inventory/get-measurements':
				$action->getMeasurement();
			break;

			case '/inventory/add-item':
				$action->addItem($_POST);
			break;

			case '/inventory/update':
				$action->update($_POST);
			break;

			case '/inventory/update-item-status':
				$action->updateItemStatus($_POST);
			break;

			
			//start of customer route	
			case '/customer/get-customers':
				$action->getCustomers();
			break;

			case '/customer/add':
				$action->add($_POST);
			break;

			case '/customer/delete':
				$action->delete($_POST);
			break;

			case '/customer/update':
				$action->update($_POST);
			break;
			
			
			//start of supplier route	
			case '/supplier/get-suppliers':
				$action->getSuppliers();
			break;

			case '/supplier/add':
				$action->add($_POST);
			break;

			case '/supplier/delete':
				$action->delete($_POST);
			break;

			case '/supplier/update':
				$action->update($_POST);
			break;
			

			//start of purchase-order route	
			case '/purchase-order/get':
				$action->get();
			break;

			case '/purchase-order/get-po':
				$action->getPO();
			break;

			case '/purchase-order/add':
				$action->add($_POST);
			break;

			case '/purchase-order/update-po':
				$action->updatePO($_POST);
			break;

			case '/purchase-order/update-lines':
				$action->updateLines($_POST);
			break;

			case '/purchase-order/add-lines':
				$action->addLines($_POST);
			break;

			case '/purchase-order/get-lines':
				$action->getLines($_POST);
			break;

			case '/purchase-order/update-status':
				$action->updateStatus($_POST);
			break;

			case '/purchase-order/delete-line':
				$action->deleteLine($_POST);
			break;

			case '/purchase-order/print':
				$action->print($_POST);
			break;

			case '/purchase-order/update':
				$action->update($_POST);
			break;

			case '/stock-transaction/get':
				$action->get($_POST);
			break;

			case '/stock-transaction/add':
				$action->add($_POST);
			break;

			case '/stock-transaction/add-lines':
				$action->addLines($_POST);
			break;

			case '/stock-transaction/print':
				$action->print($_POST);
			break;

			case '/stock-transaction/get-adjustments':
				$action->getAdjustments();
			break;

			case '/stock-transaction/adjustment':
				$action->adjustment($_POST);
			break;
			
			//start of expense route	
			case '/expense/get':
				$action->get();
			break;

			case '/expense/get-categories':
				$action->getCategory();
			break;

			case '/expense/add':
				$action->add($_POST);
			break;

			case '/expense/delete':
				$action->delete($_POST);
			break;

			//start of sales route	
			case '/sales/get':
				$action->get();
			break;

			case '/sales/add':
				$action->add($_POST);
			break;

			case '/sales/add-lines':
				$action->addLines($_POST);
			break;

			case '/sales/print':
				$action->print($_POST);
			break;

			case '/sales/cancel':
				$action->cancel($_POST);
			break;

			//reports
			case '/reports/sales':
				$action->sales($_POST);
			break;

			case '/reports/expense':
				$action->expense($_POST);
			break;

			case '/reports/inventory':
				$action->inventory($_POST);
			break;

			case '/reports/get':
				$action->get($_POST);
			break;

			case '/reports/dashboard':
				$action->dashboard();
			break;

			case '/reports/backup':
				$action->backup($_POST);
			break;

			case '/reports/get-backup':
				$action->getBackup();
			break;

			case '/reports/in':
				$action->stock($_POST);
			break;

			case '/reports/out':
				$action->stock($_POST);
			break;

			//start extra			
			case '/extra/add-measurement':
				$action->addMeasurement($_POST);
			break;	

			case '/extra/add-expense-category':
				$action->addExpenseCategory($_POST);
			break;

			default:
				Routes::handleError();
				break;
		}
	}

	public static function handleError() {
		http_response_code(404);
		$response = array();
		$response["error"] = "Resource Not Found";
		die(json_encode($response));
	}
}