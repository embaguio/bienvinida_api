<?php 
require 'vendor/autoload.php';
require_once 'vendor/dompdf/dompdf/lib/html5lib/Parser.php';
require_once 'vendor/dompdf/dompdf/src/Autoloader.php';
Dompdf\Autoloader::register();
use Dompdf\Dompdf;

require_once('SQLHelper.php');
include ('dumper.php');

class Reports {

	private $sql_obj = null;

	public function __construct(){

		$this->sql_obj = SQLHelper::get_instance();
	}

	public function getRows($data) {
		$return = array();
		$return['success'] = false;
		switch ($data['type']) {
			case 'sales':
				$sql = "CALL get_report_sales('{$data['start']}', '{$data['end']}')";
			break;

			case 'expense':
				$sql = "CALL get_report_expense('{$data['start']}', '{$data['end']}')";
			break;

			case 'inventory':
				$sql = "CALL get_report_inventory('{$data['order']}','{$data['category']}')";
			break;

			case 'in':
				$sql = "CALL get_report_stock(1, '{$data['start']}', '{$data['end']}','{$data['category']}')";
			break;

			case 'out':
				$sql = "CALL get_report_stock(2, '{$data['start']}', '{$data['end']}','{$data['category']}')";
			break;

			default:
				return;
			break;
		}

		$result = $this->sql_obj->CALL($sql);
				
		if($result){
			$final_data = array();
			while($row = mysqli_fetch_assoc($result)){
				$final_data[] = $row;
			}

			$return['success'] = $final_data && sizeof($final_data);
			$return['data'] = $final_data;
		}

		return $return;
	}


	public function sales($data) {
		$return = array();
		$return['success'] = false;
		$generatedDate = date('m/d/Y');
		$grandTotal = 0;
		$rows = $this->getRows($data)['data'];
		$buildRows = '';

		foreach($rows as $line) {
			// var_dump($line);
			// die();

			$buildRows.="
				<tr>
					<td>{$line['sales_order_id']}</td>
					<td>{$line['customer_name']}</td>
					<td>{$line['sales_order_posting']}</td>
					<td>{$line['sales_order_description']}</td>
					<td>".number_format($line['sales_order_total'], 2)."</td>
				</tr>";
			$grandTotal+=$line['sales_order_total'];
		}

		$formattedTotal = number_format($grandTotal, 2);

		$htmlString = "<!DOCTYPE html>
		<html>
			<style>
				body {
					font-family: sans-serif;
      				margin: -20px 0px;
				}
				.pull-right {
					float: right;
				}
				.gen-details span {
					margin-right: 20px;
				}
				.company {
					background: #4292bb;
					color: white;
					padding: 20px;
				}
				.header {
					font-weight: bold;
					background: #4292bb;
					color: white;

				}

				td {
					padding: 10px;
				}

				tr:nth-child(even) {background-color: #f2f2f2;}
			</style>
		<body>
		
			<h3 class='company'>Bienvinida's Sales Report <span class='pull-right'>Date Generated: {$generatedDate}</span></h3>
			<div class='gen-details'>
				<span><b>Start Date:</b> {$data['start']}</span>
				<span><b>End Date:</b> {$data['end']}</span>
			</div>
			<hr>
		
		
			<table style='width:100%'>
				<tr class='header'>
					<td>Ref #</td>
					<td>Customer Name</td>
					<td>Sales Date</td>
					<td>Description</td>
					<td>Sales Total</td>
				</tr>
				{$buildRows}
			</table>
	
			<hr>
			<p style='text-align: right;'><b>Grand Total: </b>{$formattedTotal}</p>
			
		</body>
		
		</html>";
		
		$curr_date =  date('Y-m-d');

		$dompdf = new DOMPDF();
		$dompdf->load_html($htmlString);
		$dompdf->render();
		$output = $dompdf->output();

		$saveLink = "printables/SALES-REPORT-{$curr_date}.pdf";
		file_put_contents($saveLink, $output);

		$base_url= "http://".$_SERVER['SERVER_NAME'].dirname($_SERVER['PHP_SELF']);

		$return['success'] = true;
		$return['data'] = $base_url."/".$saveLink;
				
		if ( $this->add($data['by'], 'sales', $return['data'] ) ) echo json_encode($return, JSON_UNESCAPED_SLASHES);
	}
	
	public function expense($data) {
		$return = array();
		$return['success'] = false;
		$generatedDate = date('m/d/Y');
		$grandTotal = 0;
		$rows = $this->getRows($data)['data'];
		$buildRows = '';

		foreach($rows as $line) {
			
			$buildRows.="
				<tr>
					<td>{$line['expense_id']}</td>
					<td>{$line['expense_posting']}</td>
					<td>{$line['expense_type_description']}</td>
					<td>{$line['description']}</td>
					<td>".number_format($line['expense_total'], 2)."</td>
				</tr>";
			$grandTotal+=$line['expense_total'];
		}

		$formattedTotal = number_format($grandTotal, 2);

		$htmlString = "<!DOCTYPE html>
		<html>
			<style>
				body {
					font-family: sans-serif;
      				margin: -20px 0px;
				}
				.pull-right {
					float: right;
				}
				.gen-details span {
					margin-right: 20px;
				}
				.company {
					background: #4292bb;
					color: white;
					padding: 20px;
				}
				.header {
					font-weight: bold;
					background: #4292bb;
					color: white;

				}

				td {
					padding: 10px;
				}

				tr:nth-child(even) {background-color: #f2f2f2;}
			</style>
		<body>
		
			<h3 class='company'>Bienvinida's Expense Report <span class='pull-right'>Date Generated: {$generatedDate}</span></h3>
			<div class='gen-details'>
				<span><b>Start Date:</b> {$data['start']}</span>
				<span><b>End Date:</b> {$data['end']}</span>
			</div>
			<hr>
		
		
			<table style='width:100%'>
				<tr class='header'>
					<td>Ref #</td>
					<td>Expense Date</td>
					<td>Category</td>
					<td>Description</td>
					<td>Expense Total</td>
				</tr>
				{$buildRows}
			</table>
	
			<hr>
			<p style='text-align: right;'><b>Grand Total: </b>{$formattedTotal}</p>
			
		</body>
		
		</html>";
		
		$curr_date =  date('Y-m-d');

		$dompdf = new DOMPDF();
		$dompdf->load_html($htmlString);
		$dompdf->render();
		$output = $dompdf->output();

		$saveLink = "printables/EXPENSE-REPORT-{$curr_date}.pdf";
		file_put_contents($saveLink, $output);

		$base_url= "http://".$_SERVER['SERVER_NAME'].dirname($_SERVER['PHP_SELF']);

		$return['success'] = true;
		$return['data'] = $base_url."/".$saveLink;
				
		if ( $this->add($data['by'], 'expense', $return['data'] ) ) echo json_encode($return, JSON_UNESCAPED_SLASHES);
	}

	public function inventory($data) {
		$return = array();
		$return['success'] = false;
		$generatedDate = date('m/d/Y');
		$grandTotal = 0;
		$rows = $this->getRows($data)['data'];
		$buildRows = '';

		$category = $data['category'] === '1' ? 'Raw materials' : 'Production';

		foreach($rows as $line) {
			// var_dump($line);
			// die();
			$subtotal = $line['item_price']*$line['item_inventory_qty'];
			$subtotalFormat = number_format($subtotal, 2);
			
			$buildRows.="
				<tr>
					<td>{$line['id_item_inventory']}</td>
					<td>{$line['sku']}</td>
					<td>{$line['name']}</td>
					<td>{$line['name_unit_measure']} ({$line['unit_measure_label']})</td>
					<td>{$line['item_inventory_qty']}</td>
					<td>{$line['item_price']}</td>
					<td>{$subtotalFormat}</td>
				</tr>";
				$grandTotal+=$subtotal;
				
		}
		
		$grandTotal = number_format($grandTotal, 2);

		$htmlString = "<!DOCTYPE html>
		<html>
			<style>
				body {
					font-family: sans-serif;
      				margin: -20px 0px;
				}
				.pull-right {
					float: right;
				}
				.gen-details span {
					margin-right: 20px;
				}
				.company {
					background: #4292bb;
					color: white;
					padding: 20px;
				}
				.header {
					font-weight: bold;
					background: #4292bb;
					color: white;

				}

				td {
					padding: 10px;
				}

				tr:nth-child(even) {background-color: #f2f2f2;}
			</style>
		<body>
		
			<h3 class='company'>Bienvinida's Inventory Report <span class='pull-right'>Date Generated: {$generatedDate}</span></h3>
			<div class='gen-details'>
				<span><b>{$category} : Inventory report as of today</span>
			</div>
			<hr>
		
		
			<table style='width:100%'>
				<tr class='header'>
					<td>Item ID</td>
					<td>SKU</td>
					<td>Description</td>
					<td>Unit Measure</td>
					<td>Qty</td>
					<td>Price</td>
					<td>Sub Total</td>
				</tr>
				{$buildRows}
			</table>
	
			<hr>
			<p style='text-align: right;'><b>Total Inventory Amount: </b>{$grandTotal}</p>
			
		</body>
		
		</html>";
		
		$curr_date =  date('Y-m-d');

		$dompdf = new DOMPDF();
		$dompdf->load_html($htmlString);
		$dompdf->render();
		$output = $dompdf->output();

		$saveLink = "printables/INVENTORY-REPORT-{$category}-{$curr_date}.pdf";
		file_put_contents($saveLink, $output);

		$base_url= "http://".$_SERVER['SERVER_NAME'].dirname($_SERVER['PHP_SELF']);

		$return['success'] = true;
		$return['data'] = $base_url."/".$saveLink;
				
		if ( $this->add($data['by'], 'inventory', $return['data'] ) ) echo json_encode($return, JSON_UNESCAPED_SLASHES);
	}

	
	public function stock($data) {
		$return = array();
		$return['success'] = false;
		$generatedDate = date('m/d/Y');
		$grandTotal = 0;
		$rows = $this->getRows($data)['data'];
		$buildRows = '';
		$category = $data['category'] === '1' ? 'Raw materials' : 'Production';

		foreach($rows as $line) {
			// var_dump($line);
			// die();

			$buildRows.="
				<tr>
					<td>{$line['stock_transaction_id']}</td>
					<td>{$line['stock_transaction_date']}</td>
					<td>{$line['supplier_name']}</td>
					<td>{$line['stock_transaction_description']}</td>
					<td>{$line['stock_transaction_total']}</td>
				</tr>";
				if ($line['transaction_type_id'] === '1') {
					$grandTotal+=$line['stock_transaction_total'];
				} else if ($line['transaction_type_id'] === '2') {
					$grandTotal+=$line['stock_transaction_total'];
				}
		}

		$htmlString = "<!DOCTYPE html>
		<html>
			<style>
				body {
					font-family: sans-serif;
      				margin: -20px 0px;
				}
				.pull-right {
					float: right;
				}
				.gen-details span {
					margin-right: 20px;
				}
				.company {
					background: #4292bb;
					color: white;
					padding: 20px;
				}
				.header {
					font-weight: bold;
					background: #4292bb;
					color: white;

				}

				td {
					padding: 10px;
				}

				tr:nth-child(even) {background-color: #f2f2f2;}
			</style>
		<body>
		
			<h3 class='company'>Bienvinida's Stock-{$data['type']} Report <span class='pull-right'>Date Generated: {$generatedDate}</span></h3>
			<div class='gen-details'>
			<span><b>Category:</b> {$category}</span>
				<span><b>Start Date:</b> {$data['start']}</span>
				<span><b>End Date:</b> {$data['end']}</span>
			</div>
			<hr>
		
		
			<table style='width:100%'>
				<tr class='header'>
					<td>Ref #</td>
					<td>Transaction Date</td>
					<td>Supplier</td>
					<td>Description</td>
					<td>Total</td>
				</tr>
				{$buildRows}
			</table>
	
			<hr>
			<p style='text-align: right;'><b>Stock Count: </b>{$grandTotal}</p>
			
		</body>
		
		</html>";
		
		$curr_date =  date('Y-m-d');

		$dompdf = new DOMPDF();
		$dompdf->load_html($htmlString);
		$dompdf->render();
		$output = $dompdf->output();

		$saveLink = "printables/STOCK-{$data['type']}-REPORT-{$category}-{$curr_date}.pdf";
		file_put_contents($saveLink, $output);

		$base_url= "http://".$_SERVER['SERVER_NAME'].dirname($_SERVER['PHP_SELF']);

		$return['success'] = true;
		$return['data'] = $base_url."/".$saveLink;
				
		if ( $this->add($data['by'], "stock-{$data['type']}", $return['data'] ) ) echo json_encode($return, JSON_UNESCAPED_SLASHES);
	}

	public function add($by, $type, $path){

		$sql = "CALL add_report('{$by}','{$type}','{$path}')";

		if($this->sql_obj->CALL($sql)){
			return true;
		} else {
			return false;
		}
	}

	public function get() {
		$return = array();
		$return['success'] = false;

 		$sql = "CALL get_reports()";
		$result = $this->sql_obj->CALL($sql);
		 
 		if($result){
			$final_data = array();
			while($row = mysqli_fetch_assoc($result)){
				$final_data[] = $row;
			}

 			$return['success'] = $final_data && sizeof($final_data);
 			$return['data'] = $final_data;
		}
		 
 		echo json_encode($return);
	}

	

	public function dashboard() {
		$return = array();
		$return['sales'] = 0;
		$return['stock'] = 0;
		$return['expense'] = 0;
		$return['customer'] = 0;

 		$sql = "CALL get_dashboard_sales()";
		$result = $this->sql_obj->CALL($sql);
		 
 		if($result){
			$final_data = array();
			while($row = mysqli_fetch_assoc($result)){
				$return['sales'] += $row['sales_order_total'];
			}
			$return['sales'] = number_format($return['sales'], 2);
		}

		

		$sql = "CALL get_dashboard_stock()";
		$result = $this->sql_obj->CALL($sql);
		 
 		if($result){
			$final_data = array();
			while($row = mysqli_fetch_assoc($result)){
				if ($row['stock_transaction_type'] === '1') {
					$return['stock']+=$row['stock_transaction_total'];
				} else if ($row['stock_transaction_type'] === '2') {
					$return['stock']-=$row['stock_transaction_total'];
				}
			}
		}

		$sql = "CALL get_dashboard_expenses()";
		$result = $this->sql_obj->CALL($sql);
		 
 		if($result){
			$final_data = array();
			while($row = mysqli_fetch_assoc($result)){
				$return['expense'] += $row['expense_total'];
			}
			$return['expense'] = number_format($return['expense'], 2);
		}

		$sql = "CALL get_dashboard_customer()";
		$result = $this->sql_obj->CALL($sql);
		 
 		if($result){
			$final_data = array();
			while($row = mysqli_fetch_assoc($result)){
				$return['customer'] += $row['total'];
			}
		}
		
		echo json_encode($return);
	}

	public function backup($data) {
		$return = array();
		$return['success'] = false;
		try {
			$world_dumper = Shuttle_Dumper::create(array(
				'host' => 'localhost',
				'username' => 'root',
				'password' => '',
				'db_name' => 'bienvinida',
			));
			$filename = 'backup/'.'bienvinida'. date("Y-m-d-H-i-s") .'.sql';
			$world_dumper->dump($filename);

			$file_path = "http://".$_SERVER['SERVER_NAME'].dirname($_SERVER['PHP_SELF'])."/".$filename;
			echo $file_path;
			$sql = "CALL add_backup('{$data['user']}', '{$file_path}')";

			if($this->sql_obj->CALL($sql)){
				$return['success'] = true;
			}
			
		} catch(Shuttle_Exception $e) {
			echo "Couldn't dump database: " . $e->getMessage();
		}

		echo json_encode($return);
	}

	public function getBackup() {
		$return = array();
		$return['success'] = false;

 		$sql = "CALL get_backup()";
		$result = $this->sql_obj->CALL($sql);
		 
 		if($result){
			$final_data = array();
			while($row = mysqli_fetch_assoc($result)){
				$final_data[] = $row;
			}

 			$return['success'] = $final_data && sizeof($final_data);
 			$return['data'] = $final_data;
		}
		 
 		echo json_encode($return);
	}


}



