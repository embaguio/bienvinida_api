-- Generation time: Fri, 28 Sep 2018 17:48:41 +0200
-- Host: localhost
-- DB name: bienvinida
/*!40030 SET NAMES UTF8 */;
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

DROP TABLE IF EXISTS `backup`;
CREATE TABLE `backup` (
  `backup_id` int(11) NOT NULL AUTO_INCREMENT,
  `backup_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `backup_by` int(11) NOT NULL,
  `backup_path` text COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`backup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;



DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `customer_address` text COLLATE utf8mb4_bin NOT NULL,
  `customer_contact` varchar(20) COLLATE utf8mb4_bin NOT NULL,
  `customer_is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

INSERT INTO `customer` VALUES ('1','Bienvinida\'s Sweets Shop','Airport','09123123456','0'),
('2','Test','asd','asdasdasd','1'),
('3','adasd','asdasd','asdas','1'),
('4','asdasd','asdasdas','dsadasd','1'),
('5','dasdasd','asdasd','sadasd','1'),
('6','aasd','asd','asd','1'),
('7','asd','asd','asd','1'),
('8','Bontuyan\'s Souvenir Shop','Buhangin, Davao City','123','0'),
('9','Gaisano Grocery Store','Bajada, Davao City','123456','0'),
('10','Others','N/A','N/A','0'); 


DROP TABLE IF EXISTS `expense`;
CREATE TABLE `expense` (
  `expense_id` int(11) NOT NULL AUTO_INCREMENT,
  `expense_posting` date NOT NULL,
  `expense_datecreated` datetime NOT NULL,
  `expense_type_id` int(11) NOT NULL,
  `description` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `expense_total` float NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `modified_by` int(11) NOT NULL,
  PRIMARY KEY (`expense_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

INSERT INTO `expense` VALUES ('1','2018-10-09','0000-00-00 00:00:00','1','For Durian Rolls','60000','1','1'),
('2','2018-09-27','0000-00-00 00:00:00','2','Testetes','5000','0','1'),
('3','2018-09-28','0000-00-00 00:00:00','2','test','500','0','1'); 


DROP TABLE IF EXISTS `expense_type`;
CREATE TABLE `expense_type` (
  `expense_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `expense_type_description` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`expense_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

INSERT INTO `expense_type` VALUES ('1','Cost of Goods','0'),
('2','Utility','0'),
('3','Taxes','0'),
('4','Miscellaneous','0'),
('5','Others','0'); 


DROP TABLE IF EXISTS `inventory_category`;
CREATE TABLE `inventory_category` (
  `id_inventory_category` int(11) NOT NULL AUTO_INCREMENT,
  `name_inventory_category` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id_inventory_category`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

INSERT INTO `inventory_category` VALUES ('1','Raw Materials'),
('2','Production'); 


DROP TABLE IF EXISTS `item_inventory`;
CREATE TABLE `item_inventory` (
  `id_item_inventory` int(11) NOT NULL AUTO_INCREMENT,
  `is_consumable` tinyint(1) NOT NULL,
  `sku` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `item_price` float DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `item_inventory_qty` int(11) DEFAULT NULL,
  `item_inventory_actual_measurement` float NOT NULL,
  `unit_measure` int(11) NOT NULL,
  `id_inventory_category` int(11) NOT NULL,
  `date_modified_item_inventory` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` int(11) NOT NULL,
  PRIMARY KEY (`id_item_inventory`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

INSERT INTO `item_inventory` VALUES ('1','1','45639','Sugar',NULL,'1','240','500','1','1','2018-09-23 10:56:37','1'),
('2','0','5698','Flour',NULL,'1','100','500','1','1','2018-09-23 10:56:57','1'),
('3','1','5489','Oil',NULL,'1','180','1000','1','1','2018-09-23 10:57:25','1'),
('4','0','6953','Durian Meat',NULL,'1','104','1000','1','1','2018-09-23 10:57:51','1'),
('5','1','9863','Salt',NULL,'1','50','500','1','1','2018-09-23 10:58:14','1'),
('6','1','8246','Ube Meat',NULL,'1','100','500','1','1','2018-09-23 11:00:08','1'),
('7','1','4569','Durian Pie','120','1','0','250','1','2','2018-09-23 11:00:30','1'),
('8','1','9633','Durian Tart','50','1','0','250','1','2','2018-09-23 11:00:55','1'),
('9','1','5266','Durian Polvoron','80','1','38','250','1','2','2018-09-23 11:01:26','1'),
('10','1','8596','Ube Tart','80','1','0','100','1','2','2018-09-23 11:01:51','1'),
('11','1','1234','Langka Polvoron','50','1','10','100','1','2','2018-09-23 13:00:11','1'),
('12','1','5633','Ube Pie','60','1','0','100','1','2','2018-09-23 13:01:01','1'),
('13','0','5982','Ube Cake','50','1','26','50','1','2','2018-09-23 13:03:14','1'),
('14','1','6934','Langka Tart','60','1','0','100','1','2','2018-09-23 13:04:08','1'),
('15','0','9635','Wrapper','0','1','20','65','1','1','2018-09-25 22:46:29','1'),
('16','0','4352','plate','0','1','12','12','2','1','2018-09-25 22:49:44','1'),
('17','1','1233','zxc','0','1','15','1','1','1','2018-09-25 22:50:17','1'); 


DROP TABLE IF EXISTS `purchase_order`;
CREATE TABLE `purchase_order` (
  `purchase_order_id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_id` int(11) DEFAULT NULL,
  `purchase_order_description` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `po_grand_total` float NOT NULL,
  `purchase_order_posting` date NOT NULL,
  `po_due_date` date NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `purchase_order_datemodified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` int(11) NOT NULL,
  PRIMARY KEY (`purchase_order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

INSERT INTO `purchase_order` VALUES ('1','1','For Durian Rollssss','2500','2018-09-23','2018-09-27','1','2018-09-23 11:30:59','1'),
('2','1','Test','1120','2018-09-27','2018-09-28','1','2018-09-27 21:16:09','1'); 


DROP TABLE IF EXISTS `purchase_order_line`;
CREATE TABLE `purchase_order_line` (
  `purchase_order_line_id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_order_id` int(11) NOT NULL,
  `id_item_inventory` int(11) NOT NULL,
  `expected_qty` int(11) NOT NULL,
  `received_qty` int(11) DEFAULT NULL,
  `po_unit_price` float NOT NULL,
  `po_total_price` float NOT NULL,
  PRIMARY KEY (`purchase_order_line_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

INSERT INTO `purchase_order_line` VALUES ('1','1','4','10',NULL,'100','1000'),
('2','1','3','20',NULL,'50','1000'),
('3','1','1','20',NULL,'25','500'),
('4','2','17','12',NULL,'10','120'),
('5','2','16','10',NULL,'100','1000'); 


DROP TABLE IF EXISTS `report`;
CREATE TABLE `report` (
  `report_id` int(11) NOT NULL AUTO_INCREMENT,
  `created_by` int(11) NOT NULL,
  `report_type` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `report_file_path` text COLLATE utf8mb4_bin NOT NULL,
  `report_datecreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`report_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

INSERT INTO `report` VALUES ('1','1','sales','http://localhost/files/bienvinida_api/printables/SALES-REPORT-2018-09-28.pdf','2018-09-28 19:20:49'),
('2','1','expense','http://localhost/files/bienvinida_api/printables/EXPENSE-REPORT-2018-09-28.pdf','2018-09-28 19:29:51'),
('3','1','inventory','http://localhost/files/bienvinida_api/printables/INVENTORY-REPORT-2018-09-28.pdf','2018-09-28 19:29:57'); 


DROP TABLE IF EXISTS `sales_order`;
CREATE TABLE `sales_order` (
  `sales_order_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `sales_order_description` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `sales_order_posting` date NOT NULL,
  `sales_order_total` float NOT NULL,
  `sales_order_datecreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` int(11) NOT NULL,
  `is_open` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`sales_order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

INSERT INTO `sales_order` VALUES ('1','1','Test Tarts','2018-09-23','390','2018-09-23 13:07:44','1','0'),
('2','1','Test','2018-09-23','6261.2','2018-09-23 15:15:22','1','1'),
('3','1','Test sale','2018-09-23','600','2018-09-23 16:45:35','1','1'),
('4','1','yizzzzz','2018-09-27','1300','2018-09-27 20:48:51','1','0'),
('5','1','lanaaa','2018-09-27','700','2018-09-27 20:51:14','1','0'),
('6','1','asdasd','2018-09-26','3000','2018-09-27 20:52:33','1','0'); 


DROP TABLE IF EXISTS `sales_order_line`;
CREATE TABLE `sales_order_line` (
  `sales_order_line_id` int(11) NOT NULL AUTO_INCREMENT,
  `sales_order_id` int(11) NOT NULL,
  `id_item_inventory` int(11) NOT NULL,
  `sales_line_qty` float NOT NULL,
  `sales_line_total` float NOT NULL,
  `sales_order_discount_rate` float NOT NULL,
  PRIMARY KEY (`sales_order_line_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

INSERT INTO `sales_order_line` VALUES ('1','1','8','2','50','50'),
('2','1','10','2','160','0'),
('3','1','14','3','180','0'),
('4','2','11','121','6050','0'),
('5','2','7','2','211.2','12'),
('6','3','7','10','600','50'),
('7','4','13','20','1000','0'),
('8','4','12','5','300','0'),
('9','5','14','10','600','0'),
('10','5','13','2','100','0'),
('11','6','13','10','500','0'),
('12','6','11','50','2500','0'); 


DROP TABLE IF EXISTS `stock_adjusment`;
CREATE TABLE `stock_adjusment` (
  `stock_adjusment_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_item_inventory` int(11) NOT NULL,
  `adjustment_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `system_count` int(11) NOT NULL,
  `adjustment_count` int(11) NOT NULL,
  `adjustment_remarks` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `modified_by` int(11) NOT NULL,
  PRIMARY KEY (`stock_adjusment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

INSERT INTO `stock_adjusment` VALUES ('1','1','2018-09-23 16:16:37','240','150','0','1'),
('2','1','2018-09-23 16:16:37','150','100','0','1'),
('3','1','2018-09-23 16:18:42','100','200','0','1'),
('4','1','2018-09-23 16:20:02','200','160','nagbaha nasad','1'),
('5','1','2018-09-23 16:20:35','160','200','yeyyy','1'),
('6','17','2018-09-27 14:36:32','3','40','testtttt','1'),
('7','17','2018-09-27 14:36:59','40','50','additional physical counts (????)','1'),
('8','14','2018-09-27 20:50:46','0','20','Test','1'),
('9','13','2018-09-27 20:50:51','0','30','asd','1'); 


DROP TABLE IF EXISTS `stock_transaction`;
CREATE TABLE `stock_transaction` (
  `stock_transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_transaction_date` date NOT NULL,
  `stock_transaction_last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `stock_transaction_description` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `stock_transaction_total` int(11) NOT NULL,
  `stock_transaction_type` int(11) NOT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `stock_transaction_modified_by` int(11) NOT NULL,
  PRIMARY KEY (`stock_transaction_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

INSERT INTO `stock_transaction` VALUES ('1','2018-09-27','2018-09-27 14:34:52','Test','60','1','0','1'),
('2','2018-09-27','2018-09-27 14:35:28','test','24','1','1','1'),
('3','2018-09-27','2018-09-27 14:35:40','test','12','2','0','1'),
('4','2018-09-27','2018-09-27 14:36:14','test','70','2','0','1'),
('5','2018-09-27','2018-09-27 14:38:42','For Durian Rolls','50','1','1','1'); 


DROP TABLE IF EXISTS `stock_transaction_line`;
CREATE TABLE `stock_transaction_line` (
  `stock_transaction_line_id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_transaction_id` int(11) NOT NULL,
  `id_item_inventory` int(11) NOT NULL,
  `stock_transaction_line_qty` int(11) NOT NULL,
  PRIMARY KEY (`stock_transaction_line_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

INSERT INTO `stock_transaction_line` VALUES ('1','1','4','10'),
('2','1','3','20'),
('3','1','1','20'),
('4','2','1','50'),
('5','3','1','50'),
('6','4','4','10'),
('7','4','3','20'),
('8','4','1','20'),
('9','7','9','12'),
('10','1','17','60'),
('11','2','17','24'),
('12','3','17','12'),
('13','4','17','70'),
('14','5','4','10'),
('15','5','3','20'),
('16','5','1','20'); 


DROP TABLE IF EXISTS `supplier`;
CREATE TABLE `supplier` (
  `supplier_id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_reference` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `supplier_name` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `supplier_address` text COLLATE utf8mb4_bin NOT NULL,
  `supplier_contact` varchar(20) COLLATE utf8mb4_bin NOT NULL,
  `supplier_is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`supplier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

INSERT INTO `supplier` VALUES ('0','1359','N/A','N/A','N/A','0'),
('1','4568','The Supplier','Maa Davao City','1453','0'),
('2','21323','test','asdsad','123','0'); 


DROP TABLE IF EXISTS `transaction_type`;
CREATE TABLE `transaction_type` (
  `transaction_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_type_description` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`transaction_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

INSERT INTO `transaction_type` VALUES ('1','Stock-in'),
('2','Stock-out'); 


DROP TABLE IF EXISTS `unit_of_measurement`;
CREATE TABLE `unit_of_measurement` (
  `id_unit_of_measurement` int(11) NOT NULL AUTO_INCREMENT,
  `unit_measure_label` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `name_unit_measure` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id_unit_of_measurement`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

INSERT INTO `unit_of_measurement` VALUES ('1','g','Weight'),
('2','cm','Length'),
('3','ml','Volume'); 


DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `user_password` text NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `birth_date` date NOT NULL,
  `gender` varchar(1) NOT NULL,
  `date_created_user` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated_user` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_role` int(11) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;

INSERT INTO `user` VALUES ('1','ella','dc837b0f4f0f8536f7d8e0daaba981d2dace3760','Ella Marielle','Baguio','1222-11-20','F','2018-09-02 00:00:00','2018-08-16 13:21:23','1','0'),
('27','qwe','dc837b0f4f0f8536f7d8e0daaba981d2dace3760','asd','asd','1996-05-12','M','2018-09-08 14:08:28','2018-09-08 14:08:28','1','1'),
('48','sakura','dc837b0f4f0f8536f7d8e0daaba981d2dace3760','Sakura','Haruno','1996-03-03','F','2018-09-08 16:52:05','2018-09-08 16:52:05','2','1'),
('49','bontuyan','394f2227eeb4597291135c16cc27348c75e9f39f','Queencel','Bontuyan','1990-02-15','F','2018-09-10 19:10:52','2018-09-10 19:10:52','2','1'),
('50','queencel','8cb2237d0679ca88db6464eac60da96345513964','Queencel','Bontuyan','1988-12-21','F','2018-09-19 17:35:54','2018-09-19 17:35:54','2','0'),
('51','asdasd','f10e2821bbbea527ea02200352313bc059445190','werer','werew','1996-02-12','F','2018-09-19 17:54:32','2018-09-19 17:54:32','2','1'),
('52','qwe','056eafe7cf52220de2df36845b8ed170c67e23e3','test','asdasd','2018-09-04','F','2018-09-22 16:03:19','2018-09-22 16:03:19','1','1'),
('53','','da39a3ee5e6b4b0d3255bfef95601890afd80709','','','0000-00-00','','2018-09-22 16:03:26','2018-09-22 16:03:26','0','0'); 


DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(100) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO `user_role` VALUES ('1','Administrator'),
('2','Employee'),
('3','Corporate'); 




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

