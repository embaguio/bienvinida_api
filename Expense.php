<?php 

require_once('SQLHelper.php');

class Expense {

	private $sql_obj = null;

	public function __construct(){

		$this->sql_obj = SQLHelper::get_instance();
	}

	public function get() {
		$return = array();
		$return['success'] = false;

 		$sql = "CALL get_expenses()";
		$result = $this->sql_obj->CALL($sql);
		 
 		if($result){
			$final_data = array();
			while($row = mysqli_fetch_assoc($result)){
				$final_data[] = $row;
			}

 			$return['success'] = $final_data && sizeof($final_data);
 			$return['data'] = $final_data;
		}
		 
 		echo json_encode($return);
	}


	public function add($data){
		$return = array();
		$return['success'] = false;

		$sql = "CALL add_expense('{$data['expense_posting']}', '{$data['description']}', '{$data['expense_total']}', '{$data['modified_by']}', '{$data['expense_type_id']}')";

		if($this->sql_obj->CALL($sql)){
			$return['success'] = true;
		}
		echo json_encode($return);
	}

	public function delete($data){
		$return = array();
		$return['success'] = false;

		$sql = "CALL delete_expense('".$data['expense_id']."')";

		if($this->sql_obj->CALL($sql)){
			$return['success'] = true;
		}
		echo json_encode($return);
	}

	public function getCategory() {
		$return = array();
		$return['success'] = false;

 		$sql = "CALL get_expense_types()";
		$result = $this->sql_obj->CALL($sql);
		 
 		if($result){
			$final_data = array();
			while($row = mysqli_fetch_assoc($result)){
				$final_data[] = $row;
			}

 			$return['success'] = $final_data && sizeof($final_data);
 			$return['data'] = $final_data;
		}
		 
 		echo json_encode($return);
	}
}



